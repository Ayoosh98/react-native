const API_BACKEND_URL = 'http://localhost:8080/api'


/**
 * Apis function to call the Teacher/ Student profiles.
 * @returns 
 */
export function getProfiles() {
    return fetch(`${API_BACKEND_URL}/profiles_teacher`).then((response) => response.json())
}

export function getProfile(id) {
    return fetch(`${API_BACKEND_URL}/profiles_teacher/${id}`).then((response) =>
        response.json()
    )
}

/**
 * function to post a new Profil for a teacher/ student
 * @param {*} profile 
 * @returns 
 */
export function addProfile(profile)  {
    return fetch(`${API_BACKEND_URL}/profiles_teacher`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(profile),
    }).then((response) => response.json())
}

/**
 * Function to edit a teachers or students profile
 * @param {*} id 
 * @param {*} profile 
 * @returns 
 */
export function editProfile(id, profile) {
    return fetch(`${API_BACKEND_URL}/profiles_teacher/${id}`, {
        method: 'PATCH',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(profile),
    }).then((response) => response.json())
}

/**
 * Function to delete a Teacher / Student Profile
 * @param {*} profileId 
 * @returns 
 */
export function deleteProfile(profileId) {
    return fetch(`${API_BACKEND_URL}/profiles_teacher/${profileId}`, {
        method: 'DELETE',
    }).then((response) => response.json())
}