import React from 'react';
import { StyleSheet, View, Text, StatusBar} from 'react-native';
import * as Animatable from "react-native-animatable";



/**
 * Ein Such-Screen wurde erstellt um den Nutzer suchen zu können, aber wurde bei unserem beispiel nicht verwendet.
 * durch den Tab Navigator wird es gefunden
 */
const SearchScreen = ({navigation}) => {
    return (
        <View style = {styles.container}>
        <StatusBar backgroundColor=  "#dbf9f3" barStyle='dark-content'/>
            <Animatable.View 
                animation= "fadeInUpBig"
                style = {styles.footer}
            > 
                <Text>Hier ist die Suche-Seite</Text>
            </Animatable.View>
        </View>
    );
  };

  
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#dbf9f3"
    },
    header: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 60

    },
    footer: {
        flex: 3,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 50,
        paddingHorizontal: 30

    },
    loginButton: {
        width : 110,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: "row",
        borderRadius: 20
    },

    logo: {
        width: 200,
        height: 200,
    },
    titel:{
        fontSize: 30,
        color: "#afc7c2",
        marginTop: 30
    },
    text:{
        color: 'grey',
        marginTop: 5

    },
    text_header:{
        color: '#03989e',
        fontWeight: 'bold',
        fontSize: 20
    },
    button:{
        alignItems: 'flex-end',
        marginTop: 30
    },
    textSign:{
        color: 'white',
        fontWeight: 'bold'
    }


})
export default SearchScreen;