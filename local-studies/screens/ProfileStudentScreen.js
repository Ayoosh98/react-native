import { useEffect, useState } from 'react'
import { addProfile } from './././api/express-api';
import { StyleSheet,  Text,  View, TextInput, StatusBar} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import * as Animatable from "react-native-animatable";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { SafeAreaView } from 'react-navigation';

/**
 * Profile Erstellung für Schüler*innen
 * @param {*} param0 
 * @returns 
 */

const ProfileStudentScreen = ({ navigation, profileId }) => {

    /**
     * useState hooks, um mit dem Datenbanken in Backen zu verbinden
     */
    const [name, setName] = useState('')
    const [firstName, setFirstName] = useState('')
    const [email, setEmail] = useState('')
    const [address, setAddress] = useState('')
    const [birthday, setBirthday] = useState('')
    const [phone, setPhone] = useState('')
    const [subject, setSubject] = useState('')

    /**
     * useEffect für Profil erstellen in Datenbanken im backend
     */
    useEffect(() => {
        if (!profileId) {
            return
        }
        addProfile(profileId).then((profile) => {
            setName(profile.name)
            setFirstName(profile.firstName)
            setEmail(profile.email)
            setAddress(profile.address)
            setBirthday(profile.birthday)
            setPhone(profile.phone)
            setSubject(profile.subject)
            
        })
    }, [])
    

    /**
     * Funktion, um die eingegebene Daten durch das Button speicherung in den Datenbanken speichert
     * hat zugriff auf die Funktion addProfile aus express-api.js
     * erstellt ein user und greift auf die Tabelle und verbindet es mit den Hooks
     */
    const handleSubmit = () => {
      
        console.log('handleSubmit wurde aufgerufen')

        const user = {
            name: name,
            firstName: firstName,
            email: email,
            address: address,
            money: "N/A",
            date_of_birth: birthday,
            phonenumber: phone,
            subjects: subject
        }

        /**
         * AsyncStorage is dafür gedacht, die Daten zu speichern um die mit dem getProfile wieder aufrufen können.
         */
        addProfile(user)
            .then(response => response.profile.id)
            .then(async(userId) => {
                try {
                    await AsyncStorage.setItem('userId', userId)
                  } catch (e) {
                    // saving error
                  }
            })
            .then(() => console.log(`saving completed`))
            .then(async() => {
                try {
                    const value = await AsyncStorage.getItem('userId')
                    if(value !== null) {
                    // value previously stored
                    }
                } catch(e) {
                    // error reading value
                }
            })
      
    }

return (
<View style = {styles.container}>
    <StatusBar backgroundColor=  "#dbf9f3" barStyle='dark-content'/>
        <SafeAreaView>
            <ScrollView>
                <Animatable.View 
                    animation= "fadeInUpBig"
                    style = {styles.footer}
                >
                    <Text style = {styles.text_footer}>Name</Text>
                
                    <View style = {styles.action}>
                        <FontAwesome
                            name = "user"
                            color= '#05375a'
                            size = {20}
                        />
                            <TextInput
                                id = 'name'
                                placeholder='Name'
                                style = {styles.text_input}
                                onChange={(val) => setName(val.target.value)}
                            />
                    </View>

                    <Text style = {[styles.text_footer,{
                        marginTop: 35
                    }]}>Vorname</Text>
                    <View style = {styles.action}>
                        <FontAwesome
                            name = "user"
                            color= '#05375a'
                            size = {20}
                        />
                            <TextInput
                                id = 'firstName'
                                placeholder='Vorname'
                                style = {styles.text_input}
                                onChange={(val) => setFirstName(val.target.value)}
                            />
                    </View>

                    <Text style = {[styles.text_footer,{
                        marginTop: 35
                    }]}>E-Mail</Text>

                    <View style = {styles.action}>
                        <FontAwesome
                            name= "envelope"
                            color= '#05375a'
                            size = {20}
                        />
                            <TextInput
                                id= 'email'
                                placeholder=' MaxMustermann@gmail.de'
                                style = {styles.text_input}
                                autoCapitalize='none'
                                onChange={(val) => setEmail(val.target.value)}
                            />
                    </View>

                    <Text style = {[styles.text_footer,{
                        marginTop: 35
                    }]}>Adresse</Text>

                    <View style = {styles.action}>
                        <FontAwesome
                            name= "envelope"
                            color= '#05375a'
                            size = {20}
                        />
                            <TextInput
                                id= 'address'
                                placeholder=' Mustermannstrasse 1, 11111 Berlin'
                                style = {styles.text_input}
                                autoCapitalize='none'
                                onChange={(val) => setAddress(val.target.value)}
                            />          
                    </View>
                    <Text style = {[styles.text_footer,{
                        marginTop: 35
                    }]}>Geburtsdatum</Text>

                    <View style = {styles.action}>
                        <FontAwesome
                            name= "calendar"
                            color= '#05375a'
                            size = {20}
                        />
                            <TextInput
                                id = 'birthday'
                                placeholder=' 10-01-1999'
                                style = {styles.text_input}
                                autoCapitalize='none'
                                onChange={(val) => setBirthday(val.target.value)}  
                            />
                    </View>

                    <Text style = {[styles.text_footer,{
                        marginTop: 35
                    }]}>Handynummer</Text>

                    <View style = {styles.action}>
                        <FontAwesome
                            name= "phone"
                            color= '#05375a'
                            size = {20}
                        />
                            <TextInput
                                id = 'phone'
                                placeholder='012345678910'
                                style = {styles.text_input}
                                autoCapitalize='none'
                                onChange={(val) => setPhone(val.target.value)} 
                    
                            />
                    </View>

                    <Text style = {[styles.text_footer,{
                        marginTop: 35
                    }]}>Fächer</Text>

                    <View style = {styles.action}>
                        <FontAwesome
                            name= "envelope"
                            color= '#05375a'
                            size = {20}
                        />
                            <TextInput
                                id = 'subjects'
                                placeholder='Mathe, Deutsch, Englisch'
                                style = {styles.text_input}
                                autoCapitalize='none'
                                onChange={(val) => setSubject(val.target.value)}
                            />
                    </View>

                    <View style ={styles.button}>
                        <TouchableOpacity
                            onPress= {handleSubmit}
                        >
                            <LinearGradient
                                colors={[ '#c43b5f' , '#ed547c']}
                                style = {[styles.login,{
                                    marginTop: 15
                                }]}
                            >
                                <Text style = {[styles.textSign,{
                                    color: 'white'
                                }]}>Speichern</Text>
                                
                            </LinearGradient>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress= {() => navigation.navigate('MainStudentScreen')}
                        >
                            <LinearGradient
                                colors={[ '#c43b5f' , '#ed547c']}
                                style = {[styles.login,{
                                    marginTop: 15,
                                    marginLeft: 50
                                }]}
                            >
                                <Text style = {[styles.textSign,{
                                    color: 'white'
                                }]}>Weiter</Text>
                                
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </Animatable.View>
            </ScrollView>
        </SafeAreaView>
</View>
       
);
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#dbf9f3"
    },
 
    footer: {
        flex: 3,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 20,
        paddingHorizontal: 30

    },
    text_header:{
        color: '#03989e',
        fontWeight: 'bold',
        fontSize: 20
    },
    text_footer:{
        color: '#05375a',
        fontSize: 18
    },
    logo:{
        justifyContent: 'flex-end',
        alignItems: 'stretch'
    },
    action:{
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    text_input:{
        flex: 1,
        paddingLeft: 10,
        color: '#05375a'
    },
    button: {
        flexDirection: 'row',
        marginTop: 10
        
    },
    login: {
        width : 150,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },

    textSign:{
        fontSize: 18,
        fontWeight: 'bold'
    },
    textDanger: {
        color: "#dc3545",
    },
 
})

export default ProfileStudentScreen

