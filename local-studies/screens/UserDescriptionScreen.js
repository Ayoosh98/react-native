import React, { useState } from 'react';
import { StyleSheet, View, Text, ScrollView, Image, StatusBar, Button} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as Animatable from "react-native-animatable";
import { SafeAreaView } from 'react-native-safe-area-context';
import Icons from 'react-native-vector-icons/Ionicons';
import Modal from "react-native-modal";
//import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';

/**
 * Hier wird der/die Tutor*in Seite angezeigt, indem mehr details über die Person steht.
 * @param {} param0 
 * @returns 
 */

const UserDescriptionScreen = ({navigation}) => {

    /**
     * Mit dem PopUp kann der/die Schüler*in an der/dem Tutor*in eine Anfrage schicken
     *Ende des Screens gibt es ein Button der mit dem Ratibg screen verbunden ist, damit der/die Schüler*in, den/die Tutor*in bewerten kann
     */
    const [isModalVisible, setModalVisible] = useState(false);
    const toggleModal = () => {
      setModalVisible(!isModalVisible);
    };

    /**
     * Ist für die GPS gedacht aber die Sensoren funktionieren nicht richtig, deswegen wurden die Koordinaten manuell angegeben
     */
   var location = {
       latitude: 49.005810,
       longitude: 12.063850,
       latitudeDelta: 0.009,
       longitudeDelta: 0.009
   }




return (
<View style = {styles.container}>
<StatusBar backgroundColor=  "#dbf9f3" barStyle='dark-content'/>

<SafeAreaView>
    <ScrollView>
        <Animatable.View 
        animation= "fadeInUpBig"
        style = {styles.footer}
        > 
            <View>
                <View >
                    <Text style= {{fontSize: 20, color:'black', fontWeight: 'bold'}}> Ali AlBardan</Text>
                </View>
                
                <View style= {{flexDirection:'row'}}>
                    <Icons name="ios-star"  color={'#FFA500'} size={20} />
                    <Icons name="ios-star"  color={'#FFA500'} size={20} />
                    <Icons name="ios-star"  color={'#FFA500'} size={20} />
                    <Icons name="ios-star"  color={'#FFA500'} size={20} />
                    <Icons name="ios-star-half-outline"  color={'#FFA500'} size={20} />

                    <Modal 
                        isVisible={isModalVisible}
                        transparent={true}
                    >
                        <View style={{ 
                            flex: 4, 
                            fontSize: 30,
                            height: 50,
                            marginTop: 300,
                            marginBottom: 300,
                            backgroundColor: '#e6d9c8',
                            borderTopLeftRadius: 20,
                            borderTopRightRadius: 20,
                            borderBottomLeftRadius: 20,
                            borderBottomRightRadius: 20,
                            overflow: 'hidden'
                        }}>
                            <View style = {{
                                flex: 1,
                                backgroundColor: '#e6d9c8',
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}>
                                <Text style = {{
                                        color: 'black',
                                        fontSize: 20,
                                        padding: 26,
                                }}>Anfrage wurde abgeschickt!</Text>
                            </View> 
                                <TouchableOpacity
                                    onPress={toggleModal}
                                >
                                    <LinearGradient 
                                        colors={[ '#03989e' , '#dbf9f3']}
                                        style = {styles.loginButton}
                                    >
                                        <Text style={{color: 'black', fontSize: 18}}>Schließen</Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                        </View>
                    </Modal>  
                                                
                    <TouchableOpacity onPress = {toggleModal}>
                            <LinearGradient 
                                colors={[ '#03989e' , '#dbf9f3']}
                                style = {styles.loginButton}
                            >
                                <Text style = {styles.textSign}> Anfrage senden</Text>
                                <MaterialIcons
                                    name = "navigate-next"
                                    color = 'black'
                                    size = {15}
                                />
                            </LinearGradient>
                    </TouchableOpacity>
                </View>
            
                <View>
                    <Image
                        style = {styles.logo}
                        source={require( "../assets/proband1.jpeg")}
                    />
                        <View style={{flexDirection: 'row'}}>
                            <Text style={styles.info}>Fächer:</Text>
                            <Text style = {{marginLeft: 5}}>Mathe, Deutsch</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style= {styles.info}>Beruf: </Text>
                            <Text>Student/ Tutor</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style= {styles.info}>Stundensatz:</Text>
                            <Text>12€/h</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style= {styles.info}>Entfernung:</Text>
                        </View>
                       
                       {/*
                        <View style={{flexDirection: 'row'}}>
                            <MapView
                            style = {StyleSheet.absoluteFillObject,{
                                width: 350,
                                height: 130,
                            }}
                            provider= {PROVIDER_GOOGLE}
                            mapType='hybrid'
                            region={location}
                            >
                                <Marker  
                                    coordinate={{latitude:49.005810, longitude: 12.063850}}
                                />

                            </MapView>
                        </View>
                        */}
                </View>

                <View>
                    <Text style = {styles.bewertung}>Bewertungen: </Text>
                    <Text style={{marginTop: 10}}>Er ist sehr nett und kann richtig gut erklären. Ich war am Anfang sehr verzweilft aber danach war mir alles verständlich. Danke Ali </Text>
                    <Text style= {{fontSize: 15, fontWeight: '500'}}>-Ellie Hoffmann</Text>

                    <View style= {{flexDirection:'row'}}>
                        <Icons name="ios-star"  color={'#FFA500'} size={20} />
                        <Icons name="ios-star"  color={'#FFA500'} size={20} />
                        <Icons name="ios-star"  color={'#FFA500'} size={20} />
                        <Icons name="ios-star"  color={'#FFA500'} size={20} />
                        <Icons name="ios-star"  color={'#FFA500'} size={20} />
                    </View>
                </View>

                <View>
                    <Text style={{marginTop: 10}}>Er ist sehr nett aber manchmal war er leicht verwirrt aber kann richtig gut erklären. Ich war am Anfang sehr verzweilft aber danach war mir alles verständlich. Danke Ali </Text>
                    <Text style= {{fontSize: 15, fontWeight: '500'}}>-Daniel Schneider</Text>

                    <View style= {{flexDirection:'row'}}>
                        <Icons name="ios-star"  color={'#FFA500'} size={20} />
                        <Icons name="ios-star"  color={'#FFA500'} size={20} />
                        <Icons name="ios-star"  color={'#FFA500'} size={20} />
                        <Icons name="ios-star"  color={'#FFA500'} size={20} />
                        <Icons name="ios-star-outline"  color={'#FFA500'} size={20} />
                    </View>
                </View>
                <TouchableOpacity 
                    onPress= {() => navigation.navigate('RatingScreen')}
                >
                    <LinearGradient 
                        colors={[ '#03989e' , '#dbf9f3']}
                        style = {{
                            width : 150,
                            height: 50,
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: "row",
                            borderRadius: 20,
                            marginLeft: 220,
                        }}
                    >
                        <Text style = {styles.textSign}> bewerten</Text>
                        <MaterialIcons
                            name = "navigate-next"
                            color = 'black'
                            size = {15}
                        />
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        </Animatable.View>
    </ScrollView> 
</SafeAreaView>
</View>       
    );
  };

  
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#dbf9f3"
    },
    header: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 60

    },
    footer: {
        flex: 3,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 50,
        paddingHorizontal: 30

    },
    loginButton: {
        width : 150,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: "row",
        borderRadius: 20,
        marginLeft: 120,
        marginBottom: 10
    
    },

    logo: {
        width: 250,
        height: 300,
        marginBottom: 10
    },
    titel:{
        fontSize: 30,
        color: "#afc7c2",
        marginTop: 30
    },
    text:{
        color: 'grey',
        marginTop: 5,
        marginBottom: 10

    },
    text_header:{
        color: '#03989e',
        fontWeight: 'bold',
        fontSize: 20
    },
    button:{
        alignItems: 'flex-end',
        marginTop: 30
    },
    textSign:{
        color: 'black',
        fontWeight: 'bold',
 
    },
    bewertung:{
        fontSize: 15,
        fontWeight: 'bold',
        marginTop: 30
    },
    info:{
        fontSize: 15,
        fontWeight:'bold',

    }

})
export default UserDescriptionScreen;