import React, { useState } from 'react';
import { StyleSheet, View, Text, ScrollView, Image, StatusBar} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import * as Animatable from "react-native-animatable";
import { SafeAreaView } from 'react-native-safe-area-context';
import Icons from 'react-native-vector-icons/Ionicons';
import Modal from "react-native-modal";
//import StarRating from 'react-native-star-rating';

const RatingsScreen = ({navigation}) => {

    /**
     * useState um, die PopUp zu aktivieren und wieder schließen bei dem Button Abschicken
     */
    const [isModalVisible, setModalVisible] = useState(false);
    const toggleModal = () => {
      setModalVisible(!isModalVisible);
    };

    const [rating, setRating] = useState({
        starCount: 2.5,
        disabled: '',
    })

    /**
     * hier durch werden die Sterne dann gedrückt mit dem press Funktion
     */
    const onStarRatingPress = (ratings) =>{
        setRating({
            starCount: ratings
        });
    }

    return (
        <View style = {styles.container}>
            <StatusBar backgroundColor=  "#dbf9f3" barStyle='dark-content'/>
                <SafeAreaView>
                    <ScrollView>
                        <Animatable.View 
                            animation= "fadeInUpBig"
                            style = {styles.footer}
                        >
                                <View>
                                    <View >
                                        <Text style= {{fontSize: 20, color:'black', fontWeight: 'bold'}}> Ali AlBardan</Text>
                                    </View>
                                    
                                    <View style= {{flexDirection:'row'}}>
                                        <Icons name="ios-star"  color={'#FFA500'} size={20} />
                                        <Icons name="ios-star"  color={'#FFA500'} size={20} />
                                        <Icons name="ios-star"  color={'#FFA500'} size={20} />
                                        <Icons name="ios-star"  color={'#FFA500'} size={20} />
                                        <Icons name="ios-star-half-outline"  color={'#FFA500'} size={20} />
    
                                        <Modal 
                                            isVisible={isModalVisible}
                                            transparent={true}
                                        >
                                            <View style={{ 
                                                flex: 4, 
                                                fontSize: 30,
                                                height: 50,
                                                marginTop: 300,
                                                marginBottom: 300,
                                                backgroundColor: '#e6d9c8',
                                                borderTopLeftRadius: 20,
                                                borderTopRightRadius: 20,
                                                borderBottomLeftRadius: 20,
                                                borderBottomRightRadius: 20,
                                                overflow: 'hidden'
                                            }}>
                                                <View style = {{
                                                    flex: 1,
                                                    backgroundColor: '#e6d9c8',
                                                    alignItems: 'center',
                                                    justifyContent: 'center'
                                                }}>
                                                    <Text style = {{
                                                            color: 'black',
                                                            fontSize: 20,
                                                            padding: 26,
                                                            fontWeight: 'bold'
                                                    }}>Bewertung wurde Abgeschickt!</Text>
                                                    <Text style = {{
                                                        color: 'black',
                                                        fontSize: 18,
    
                                                    }}>Danke für Ihre Bewertung. Ihre Bewertung ist uns sehr wichtig und werden uns an Ihrem Feedback beachten.</Text>
                                                    
                                                </View> 
                                                    <TouchableOpacity
                                                        onPress={toggleModal}
                                                    >
                                                        <LinearGradient 
                                                            colors={[ '#03989e' , '#dbf9f3']}
                                                            style = {styles.loginButton}
                                                        >
                                                            <Text style={{color: 'black', fontSize: 18}}>Schließen</Text>
                                                        </LinearGradient>
                                                    </TouchableOpacity>
                                            </View>
                                        </Modal>  
                                                                    
                                        <TouchableOpacity onPress = {toggleModal}>
                                                <LinearGradient 
                                                    colors={[ '#03989e' , '#dbf9f3']}
                                                    style = {styles.loginButton}
                                                >
                                                    <Text style = {styles.textSign}> Abschicken</Text>
                                                    <MaterialIcons
                                                        name = "navigate-next"
                                                        color = 'black'
                                                        size = {15}
                                                    />
                                                </LinearGradient>
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <Image
                                            style = {styles.logo}
                                            source={require( "../assets/proband1.jpeg")}
                                        />
                                    </View>
                                
                                    <View >
                                        <Text style= {{fontSize: 15, color:'black', fontWeight: 'bold', marginTop: 20}}> Wie viele Sterne möchtest du geben?</Text>
                                    </View>

                                     {/*<View style= {{flexDirection:'row'}}>
                                    <StarRating
                                        disabled={false}
                                        emptyStar={'ios-star-outline'}
                                        fullStar={'ios-star'}
                                        halfStar={'ios-star-half'}
                                        iconSet={'Ionicons'}
                                        maxStars={5}
                                        rating={rating.starCount}
                                        selectedStar={(rating) => onStarRatingPress(rating)}
                                        fullStarColor={'#FFA500'}
                                        starSize={25}
                                    />
                                    /View>
                                     */}
                                <TouchableOpacity 
                                    onPress= {() => navigation.navigate('UserDescriptionScreen')}
                                >
                                            <LinearGradient 
                                                colors={[ '#03989e' , '#dbf9f3']}
                                                style = {{
                                                    width : 150,
                                                    height: 50,
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    flexDirection: "row",
                                                    borderRadius: 20,
                                                    marginLeft: 220,
                                                }}
                                            >
                                                <Text style = {styles.textSign}> Back</Text>
                                                <MaterialIcons
                                                    name = "navigate-next"
                                                    color = 'black'
                                                    size = {15}
                                                />
                                            </LinearGradient>
                                </TouchableOpacity>
                            </View>
                    </Animatable.View>
                </ScrollView> 
            </SafeAreaView> 
    </View>
);
};

  
 
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#dbf9f3"
    },
    header: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 60

    },
    footer: {
        flex: 3,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 50,
        paddingHorizontal: 30

    },
    loginButton: {
        width : 150,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: "row",
        borderRadius: 20,
        marginLeft: 120,
        marginBottom: 10
    
    },

    logo: {
        width: 250,
        height: 300,
        marginBottom: 10
    },
    titel:{
        fontSize: 30,
        color: "#afc7c2",
        marginTop: 30
    },
    text:{
        color: 'grey',
        marginTop: 5,
        marginBottom: 10

    },
    text_header:{
        color: '#03989e',
        fontWeight: 'bold',
        fontSize: 20
    },
    button:{
        alignItems: 'flex-end',
        marginTop: 30
    },
    textSign:{
        color: 'black',
        fontWeight: 'bold',
 
    },
    bewertung:{
        fontSize: 15,
        fontWeight: 'bold',
        marginTop: 30
    },
    info:{
        fontSize: 15,
        fontWeight:'bold',

    }


})

export default RatingsScreen;
