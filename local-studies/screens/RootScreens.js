import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import WelcomeScreen from './WelcomeScreen';
import LogInScreen from './LogInScreen';
import SignUpScreen from './SignUpScreen';
import PositionScreen from './PositionScreen';
import ProfileTeacherScreen from './ProfileTeacherScreen';
import ProfileStudentScreen from './ProfileStudentScreen';
import TabScreen from './TabScreen';
import UserDescriptionScreen from './UserDescriptionScreen';
import ImpressumScreen from './ImpressumScreen';
import SearchScreen from './SearchScreen';

/**
 * Mit dem Root Screen ermöglichen wir durch den Stack navigator ein ordentliche View über die App.js
 * hier werden alle screens mit dem Stack navigator verbunden
 */
const RootStack = createStackNavigator();

const RootScreens = ({navigation}) => (
   <RootStack.Navigator screenOptions={{
    headerTintColor: '#03989e',
    headerStyle:{
        backgroundColor: '#dbf9f3',
    }
   }}>
       <RootStack.Screen name= "WelcomeScreen" component= {WelcomeScreen} options={{
           headerShown: false,
    }}/>
       <RootStack.Screen name= "LogInScreen" component= {LogInScreen} options={{
             title: 'Sign-In',
             headerLeft: null,
            headerShown: false
   }}/>
       <RootStack.Screen name= "SignUpScreen" component= {SignUpScreen} options={{
            title: 'Sign-Up',
            headerLeft: null,
            headerShown: false
   }}/>
       <RootStack.Screen name= "PositionScreen" component= {PositionScreen} options={{
            headerShown: false,
   }}/>
       <RootStack.Screen name= "ProfileTeacherScreen"  component= {ProfileTeacherScreen}  options={{
            headerLeft: null,
            title: 'Profilerstellung',
            headerShown: true
   }}/>
       <RootStack.Screen name= "ProfileStudentScreen"  component= {ProfileStudentScreen} options={{
            headerLeft: null,
            title: 'Profil erstellen',
            headerShown: true
   }}/>

      <RootStack.Screen name= "MainStudentScreen" component= {TabScreen} options={{
          headerLeft: null,
          title: 'Startseite',
          headerShown: true
   }}/>
   <RootStack.Screen name= "UserDescriptionScreen" component= {UserDescriptionScreen} options={{
            headerLeft: null,
            title: 'Profil',
            headerShown: true,
   }}/>
       <RootStack.Screen name= "ImpressumScreen" component= {ImpressumScreen} options={{
            title: 'Impressum',
            headerShown: true
   }}/>
        <RootStack.Screen name= "SearchScreen" component= {SearchScreen} options={{
            headerLeft: null,
            title: 'Suche',
            headerShown: true,
            headerBackTitleVisible: null
   }}/>
    
   </RootStack.Navigator>
);

export default RootScreens;