import React, {useState} from 'react';
import { StyleSheet, Text, View, TextInput, StatusBar} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as Animatable from "react-native-animatable";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';


const LogInScreen = ({navigation}) => {
    /**
     * useState hooks.
     * if the name/ email are empty it would not show any icons but as soon as we type in anything, we'd get a kind of validation
     * as a green check circle icon
     * 
     * as well the eye-off to secure the entry of the Password
     * 
     */

    const [data, setData] = useState({
        email: '',
        password: '',
        check_textInputChange: false,
        secureTextEntry: true

    });
    
    /**
     * Checks the Email
     * @param {*} val 
     */
    const textInputChange = (val) =>{
        if(val.length != 0){
            setData({
                ...data,
                email: val,
                check_textInputChange: true
            });
        }else{
            setData({
                ...data,
                email: val,
                check_textInputChange: false
            });
        }
    }

    /**
     * Checks the Password
     * @param {*} val 
     */
    const handlePasswordChange = (val) =>{
        setData ({
            ...data,
            password: val
        });
    }
    const updateSecureTextEntry = () =>{
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }

    return (
    <View style = {styles.container}>
    <StatusBar backgroundColor= "#dbf9f3" barStyle='dark-content'/>
        
    <View style = {styles.header}>
        <Text style = {styles.text_header}>Welcome!</Text>
    </View>
        
    <Animatable.View
        animation= "fadeInUpBig"
        style = {styles.footer}
    >
        <Text style = {styles.text_footer}>E-Mail</Text>
        <View style = {styles.action}>
            <FontAwesome
                name= "envelope"
                color= '#05375a'
                size = {20}
            />
                <TextInput
                    placeholder=' MaxMustermann@gmail.de'
                    style = {styles.text_input}
                    onChangeText={(val) => textInputChange(val)}
                />
            {data.check_textInputChange ?
            <Feather
                name='check-circle'
                color= 'green'
                size={20}
            />
            : null}
        </View>

        <Text style = {styles.text_footer,{
        marginTop: 35
        }}>Password</Text>
        <View style = {styles.action}>
            <FontAwesome
                name= "lock"
                color= '#05375a'
                size = {20}
            />
                <TextInput
                    placeholder=' Password'
                    secureTextEntry = {data.secureTextEntry ? true : false}
                    style = {styles.text_input}
                    autoCapitalize='none'
                    onChangeText={(val) => handlePasswordChange(val)}
                />
      
            <TouchableOpacity
                onPress={updateSecureTextEntry}
            >
                {data.secureTextEntry ?
                <Feather
                    name='eye-off'
                    color= 'grey'
                    size={20}
                />
                :
                <Feather
                    name='eye'
                    color= 'grey'
                    size={20}
                />
                }
            </TouchableOpacity>
        </View>

        <View style ={styles.button}>
            <TouchableOpacity
             onPress={() => navigation.navigate('MainStudentScreen')}
            >
                <LinearGradient
                    colors={[ '#c43b5f' , '#ed547c']}
                    style = {styles.login}
                >
                    <Text style = {[styles.textSign,{
                    color: 'white'
                    }]}>Sign In</Text>
                 </LinearGradient>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => navigation.navigate('SignUpScreen')}
                style ={[styles.login, {
                borderColor: '#c43b5f',
                borderWidth: 1,
                marginTop: 15
                }]}
            >
                <Text style = {[styles.textSign,{
                color: '#c43b5f'
                }]}>Sign Up</Text>
            </TouchableOpacity>
        </View>
    </Animatable.View>
    </View>
  );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#dbf9f3"
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50

    },
    footer: {
        flex: 3,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 20,
        paddingHorizontal: 30

    },
    text_header:{
        color: '#03989e',
        fontWeight: 'bold',
        fontSize: 30
    },
    text_footer:{
        color: '#05375a',
        fontSize: 18
    },
    logo:{
        justifyContent: 'flex-end',
        alignItems: 'stretch'
    },
    action:{
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    text_input:{
        flex: 1,
        paddingLeft: 10,
        color: '#05375a'
    },
    button: {
        alignItems: 'center',
        marginTop: 50
    },
    login: {
        width : 350,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },

    textSign:{
        fontSize: 18,
        fontWeight: 'bold'
    }


})

export default LogInScreen;