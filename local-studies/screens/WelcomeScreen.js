import React from 'react';
import { StyleSheet, View, Text, StatusBar} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as Animatable from "react-native-animatable";

/**
 * Welcome Screen ist mit dem Logo und ein Button , umd mit dem dann Login und Registrieren starten können.
 */
const WelcomeScreen = ({navigation}) => {
    return (
        <View style ={styles.container}>
        <StatusBar backgroundColor=  "#dbf9f3" barStyle='dark-content'/>

            <View style = {styles.header}>
                <Animatable.Image 
                    animation= "bounce"
                    duration={3000}
                    style = {styles.logo} 
                    source={require( "../assets/logo2.png")}
                    resizeMode ="stretch"
                />
            </View>
            <Animatable.View 
                style = {styles.footer}
                animation= "fadeInUpBig"
            >
                <Animatable.Text 
                   animation="rubberBand" duration = {1500} style = {styles.titel}> Welcome to Local Studies!
                </Animatable.Text>
                
                    <Text style = {styles.text}> Sign in with your account</Text>
                <View style = {styles.button}>
                    <TouchableOpacity onPress = {() => navigation.navigate("LogInScreen")}>
                        <LinearGradient 
                            colors={[ '#c43b5f' , '#ed547c']}
                            style = {styles.loginButton}
                        >
                            <Text style = {styles.textSign}> Get Started</Text>
                            <MaterialIcons
                                name = "navigate-next"
                                color = 'white'
                                size = {15}
                            />
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </Animatable.View>
        </View>
    );
  };

  
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#dbf9f3"
    },
    header: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center'

    },
    footer: {
        flex: 1,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 50,
        paddingHorizontal: 30

    },
    loginButton: {
        width : 110,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: "row",
        borderRadius: 20
    },

    logo: {
        width: 200,
        height: 200,
    },
    titel:{
        fontSize: 30,
        color: "#afc7c2",
        marginTop: 30
    },
    text:{
        color: 'grey',
        marginTop: 5

    },
    button:{
        alignItems: 'flex-end',
        marginTop: 30
    },
    textSign:{
        color: 'white',
        fontWeight: 'bold'
    }


})
export default WelcomeScreen