import React, {useState} from 'react';
import { StyleSheet,  Text,  View, TextInput, StatusBar, Button} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as Animatable from "react-native-animatable";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';



const SignUpScreen = ({navigation}) => {
    
    /**
     * useState to hook react functions with normal functions.
     * if the name/ email are empty it would not show any icons but as soon as we type in anything, we'd get a kind of validation
     * as a green check circle icon
     * 
     * as well the eye-off to secure the entry of the Password
     * 
     * we also have states for the form validation to confirm the Password 
     */
    const [user, setUser] = useState({
        name: '',
        firstName: '',
        email: '',
        password: '',

        passwordError:'',
        confirmPassword: '',
        confirmPasswordError: '',
        confirmedMes: '',

        check_textInputChange: false,
        check_nameTextInputChange: false,
        secureTextEntry: true,
        secureConfirmEntry: true,
        
    });

/**
 * checks if there is input in the email field
 * @param {*} val 
 */
    const textInputChange = (val) =>{
        if(val.length != 0){
            setUser({
                ...user,
                email: val,
                check_textInputChange: true
            });
        }else{
            setUser({
                ...user,
                email: val,
                check_textInputChange: false
            });
        }
    }
    const nameTextInputChange = (val) =>{
        if(val.length != 0){
            setUser({
                ...user,
                name: val,
                check_nameTextInputChange: true
            });
        }else{
            setUser({
                ...user,
                name: val,
                check_nameTextInputChange: false
            });
        }
    }
    const firstnameTextInputChange = (val) =>{
        if(val.length != 0){
            setUser({
                ...user,
                firstName: val,
                check_firstnameTextInputChange: true
            });
        }else{
            setUser({
                ...user,
                firstName: val,
                check_firstnameTextInputChange: false
            });
        }
    }

/**
 * the eye-off so the password remain unseen and the eye so the user can check for mistakes
 * @param {} val 
 */
    const handlePasswordChange = (val) =>{
        setUser ({
            ...user,
            password: val
        });
    }
    const handleConfirmPasswordChange = (val) =>{
        setUser ({
            ...user,
            confirmPassword: val
        });
    }
 
    const updateSecureTextEntry = () =>{
        setUser({
            ...user,
            secureTextEntry: !user.secureTextEntry
        });
    }
    const updateConfirmSecureTextEntry = () =>{
        setUser({
            ...user,
            secureConfirmEntry: !user.secureConfirmEntry
        });
    }

    /**
     * Function to validate the Password
     */
    const formValidation = async () =>{

        if(user.confirmPassword.length == 0){
            setUser({
                ...user,
                confirmPasswordError: 'Password is required field'
            });

        }else if(user.password !== user.confirmPassword){
            setUser({
                ...user,
                confirmPasswordError: 'Password and confirm Password should be the same.'
            });
        } else if(user.password.length !== 0 && user.confirmPassword.length !== 0 && user.password == user.confirmPassword){
            setUser({
                ...user,
                confirmPasswordError:'Passwords are Confirmed. You can Sign Up'
            });
        }

    }
  


return (
<View style = {styles.container}>
    <StatusBar backgroundColor=  "#dbf9f3" barStyle='dark-content'/>

        <View style = {styles.header}>
            <Text style = {styles.text_header}>Register Now!</Text>
        </View>
        <Animatable.View 
        animation= "fadeInUpBig"
        style = {styles.footer}
        >
            <Text style = {styles.text_footer}>Name</Text>
            <View style = {styles.action}>
                <FontAwesome
                    name = "user"
                    color= '#05375a'
                    size = {20}
                />
                    <TextInput
                        placeholder='Name'
                        style = {styles.text_input}
                        onChangeText={(val) => nameTextInputChange(val)}
                    />
                {user.check_nameTextInputChange ? 
                    <Feather
                        name='check-circle'
                        color= 'green'
                        size={20}
                    />
                : null
                }

            </View>

            <Text style = {[styles.text_footer,{
                marginTop: 35
            }]}>Vorname</Text>
            <View style = {styles.action}>
                <FontAwesome
                    name = "user"
                    color= '#05375a'
                    size = {20}
                />
                    <TextInput
                        placeholder='Vorname'
                        style = {styles.text_input}
                        onChangeText={(val) => firstnameTextInputChange(val)}
                    />
                {user.check_firstnameTextInputChange ? 
                    <Feather
                        name='check-circle'
                        color= 'green'
                        size={20}
                    />
                : null}
            </View>

            <Text style = {[styles.text_footer,{
                marginTop: 35
            }]}>E-Mail</Text>

            <View style = {styles.action}>
                <FontAwesome
                    name= "envelope"
                    color= '#05375a'
                    size = {20}
                />
                    <TextInput
                        placeholder=' MaxMustermann@gmail.de'
                        style = {styles.text_input}
                        autoCapitalize='none'
                        onChangeText={(val) => textInputChange(val)}
                    />
                {user.check_textInputChange ? 
                    <Feather
                        name='check-circle'
                        color= 'green'
                        size={20}
                    />
                : null}
            </View>

            <Text style = {[styles.text_footer,{
                marginTop: 35
            }]}>Password</Text>

            <View style = {styles.action}>
                <FontAwesome
                    name= "lock"
                    color= '#05375a'
                    size = {20}
                />
                    <TextInput
                        placeholder=' Password'
                        secureTextEntry = {user.secureTextEntry ? true : false}
                        style = {styles.text_input}
                        autoCapitalize='none'
                        onChangeText={(val) => handlePasswordChange(val)}
                    />
            
                <TouchableOpacity
                    onPress={updateSecureTextEntry}
                >
                {user.secureTextEntry ?
                    <Feather
                        name='eye-off'
                        color= 'grey'
                        size={20}
                    />
                    : 
                    <Feather
                        name='eye'
                        color= 'grey'
                        size={20}
                    />
                }
                </TouchableOpacity>
            </View>

            <Text style = {[styles.text_footer,{
                marginTop: 35
            }]}>Confirm Password</Text>

            <View style = {styles.action}>
                <FontAwesome
                    name= "lock"
                    color= '#05375a'
                    size = {20}
                />
                    <TextInput
                        placeholder=' Confirm Password'
                        secureTextEntry = {user.secureConfirmEntry ? true : false}
                        style = {styles.text_input}
                        autoCapitalize='none'
                        onChangeText={(val) => handleConfirmPasswordChange(val)}
                    />

                <TouchableOpacity
                    onPress={updateConfirmSecureTextEntry}
                >
                {user.secureConfirmEntry ?
                    <Feather
                        name='eye-off'
                        color= 'grey'
                        size={20}
                    />
                    : 
                    <Feather
                        name='eye'
                        color= 'grey'
                        size={20}
                    />
                }
                </TouchableOpacity>
            </View>

            <View style ={styles.button}>
                {user.confirmPasswordError.length > 0 && <Text style={styles.textDanger}>{user.confirmPasswordError}</Text>}
                <TouchableOpacity 
                    style ={[styles.login, {
                            borderColor: '#c43b5f',
                            borderWidth: 1,
                            marginTop: 15
                        }]}
                    onPress={formValidation}  
                > 
                <Text  style = {[styles.textSign,{
                        color: '#c43b5f'
                    }]}>Confirm</Text> 

                </TouchableOpacity>

                <TouchableOpacity
                    onPress= {() => navigation.navigate('PositionScreen')}
                >
                    <LinearGradient
                        colors={[ '#c43b5f' , '#ed547c']}
                        style = {[styles.login,{
                            marginTop: 15
                        }]}
                    >
                    
                        <Text style = {[styles.textSign,{
                            color: 'white'
                        }]}>Sign Up</Text>
                    
                    </LinearGradient>
                </TouchableOpacity>
            </View>
            
        </Animatable.View>
</View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#dbf9f3"
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50

    },
    footer: {
        flex: 5,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 20,
        paddingHorizontal: 30

    },
    text_header:{
        color: '#03989e',
        fontWeight: 'bold',
        fontSize: 30,
    },
    text_footer:{
        color: '#05375a',
        fontSize: 18
    },
    logo:{
        justifyContent: 'flex-end',
        alignItems: 'stretch'
    },
    action:{
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    text_input:{
        flex: 1,
        paddingLeft: 10,
        color: '#05375a'
    },
    button: {
        alignItems: 'center',
        marginTop: 10
        
    },
    login: {
        width : 350,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },

    textSign:{
        fontSize: 18,
        fontWeight: 'bold'
    },
    textDanger: {
        color: "#dc3545",
    },
 

})

export default SignUpScreen;