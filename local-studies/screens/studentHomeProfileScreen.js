import { useEffect, useState } from 'react'
import { StyleSheet,  Text,  View, TextInput, StatusBar} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import * as Animatable from "react-native-animatable";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {addProfile, getProfile, deleteProfile, editProfile} from './././api/express-api';
import { SafeAreaView } from 'react-navigation';

/**
 * Schüler*in Profil über die Tab navigator in main screen. 
 * Hier wurden drei buttons mit dem Apis verbunden. einmal löschen , Aktualisierung sowie Bearbeitung Buttons
 */

const studentHomeProfileScreen = ({ navigation, profileId }) => {
    const [name, setName] = useState('')
    const [firstName, setFirstName] = useState('')
    const [email, setEmail] = useState('')
    const [address, setAddress] = useState('')
    const [birthday, setBirthday] = useState('')
    const [phone, setPhone] = useState('')
    const [subject, setSubject] = useState('')

    useEffect(() => {
        if (!profileId) {
            return
        }
        addProfile(profileId).then((profile) => {
            setName(profile.name)
            setFirstName(profile.firstName)
            setEmail(profile.email)
            setAddress(profile.address)
            setBirthday(profile.birthday)
            setPhone(profile.phone)
            setSubject(profile.subject)
            
        })
    }, [])
    

    /**
     * Mit dem handleSubmit konstante rufen wir die gespeicherte Daten von den APis auf und speichern wir die in den Feldern
     */
    const handleSubmit = async () => {
        try {
            const userId = await AsyncStorage.getItem('userId')
            if(userId === null) {
                console.log(`User id is missing`);
            }

            getProfile(userId)
            .then(response => {
                console.log(response)
                setName( response.name)
                setFirstName(response.firstName)
                setEmail(response.email)
                setAddress(response.address)
                setBirthday(response.date_of_birth)
                setPhone(response.phonenumber)
                setSubject(response.subjects)
            })
        } catch(e) {
            // error reading value
            console.log(`error occurred`);
        }
      
    }

    /**
     * Mit dem deleteSubmit, löschen wir die Daten im Profil, sowie in dem Api
     */
    const deleteSubmit = async () => {
        try {
            const userId = await AsyncStorage.getItem('userId')
            if(userId === null) {
                console.log(`User id is missing`);
            }

            deleteProfile(userId)
            .then(response => {
                console.log(response)
                setName( "")
                setFirstName("")
                setEmail("")
                setAddress("")
                setBirthday("")
                setPhone("")
                setSubject("")
            })
        } catch(e) {
            // error reading value
            console.log(`error occurred`);
        }
      
    }
    /**
     * Mit dem editSubmit, können wir die Daten bearbeiten und noch mal mit dem aktualisierung button aufrufen und in den datenbanken speichern
     */
    const editSubmit = async () => {
        const user = {
            name: name,
            firstName: firstName,
            email: email,
            address: address,
            money: "N/A",
            date_of_birth: birthday,
            phonenumber: phone,
            subjects: subject
        }

        try {
            const userId = await AsyncStorage.getItem('userId')
            if(userId === null) {
                console.log(`User id is missing`);
            }

            editProfile(userId, user)
            .then(response => {
                console.log(response)
                setName( response.name)
                setFirstName(response.firstName)
                setEmail(response.email)
                setAddress(response.address)
                setBirthday(response.date_of_birth)
                setPhone(response.phonenumber)
                setSubject(response.subjects)
            })
        } catch(e) {
            // error reading value
            console.log(`error occurred`);
        }       
      
    }

    return (
    <View style = {styles.container}>
    <StatusBar backgroundColor=  "#dbf9f3" barStyle='dark-content'/>

        <View style= {{
            marginLeft: 270
        }}>
            <TouchableOpacity
                onPress={handleSubmit}
            >
                    <Text style = {[styles.textSign,{
                        color: 'black'
                    }]}>Aktualisieren</Text>
                    
            </TouchableOpacity>
        </View>

        <SafeAreaView>
            <ScrollView>
                <Animatable.View 
                    animation= "fadeInUpBig"
                    style = {styles.footer}
                >
                    <Text style = {styles.text_footer}>Name</Text>
                    <View style = {styles.action}>
                        <FontAwesome
                            name = "user"
                            color= '#05375a'
                            size = {20}
                        />
                            <TextInput
                                id = 'name'
                                placeholder='Name'
                                style = {styles.text_input}
                                onChange={(val) => setName(val.target.value)}
                                defaultValue= {name}
                            />
                    </View>

                    <Text style = {[styles.text_footer,{
                        marginTop: 35
                    }]}>Vorname</Text>
                    <View style = {styles.action}>
                        <FontAwesome
                            name = "user"
                            color= '#05375a'
                            size = {20}
                        />
                            <TextInput
                                id = 'firstName'
                                placeholder='Vorname'
                                style = {styles.text_input}
                                onChange={(val) => setFirstName(val.target.value)}
                                defaultValue={firstName}            
                            />
                    
                    </View>

                    <Text style = {[styles.text_footer,{
                        marginTop: 35
                    }]}>E-Mail</Text>

                    <View style = {styles.action}>
                        <FontAwesome
                            name= "envelope"
                            color= '#05375a'
                            size = {20}
                        />
                            <TextInput
                                id= 'email'
                                placeholder=' MaxMustermann@gmail.de'
                                style = {styles.text_input}
                                autoCapitalize='none'
                                onChange={(val) => setEmail(val.target.value)}   
                                defaultValue= {email}             
                            />
                
                    </View>

                    <Text style = {[styles.text_footer,{
                        marginTop: 35
                    }]}>Adresse</Text>

                    <View style = {styles.action}>
                        <FontAwesome
                            name= "envelope"
                            color= '#05375a'
                            size = {20}
                        />
                            <TextInput
                                id= 'address'
                                placeholder=' Mustermannstrasse 1, 11111 Berlin'
                                style = {styles.text_input}
                                autoCapitalize='none'
                                onChange={(val) => setAddress(val.target.value)}
                                defaultValue={address}
                            />          
                    </View>

                    <Text style = {[styles.text_footer,{
                        marginTop: 35
                    }]}>Geburtsdatum</Text>
                    
                    <View style = {styles.action}>
                        <FontAwesome
                            name= "calendar"
                            color= '#05375a'
                            size = {20}
                        />
                            <TextInput
                                id = 'birthday'
                                placeholder=' 10-01-1999'
                                style = {styles.text_input}
                                autoCapitalize='none'
                                onChange={(val) => setBirthday(val.target.value)}      
                                defaultValue={birthday}
                            />
                    </View>

                    <Text style = {[styles.text_footer,{
                        marginTop: 35
                    }]}>Handynummer</Text>

                    <View style = {styles.action}>
                        <FontAwesome
                            name= "phone"
                            color= '#05375a'
                            size = {20}
                        />
                            <TextInput
                                id = 'phone'
                                placeholder='012345678910'
                                style = {styles.text_input}
                                autoCapitalize='none'
                                onChange={(val) => setPhone(val.target.value)}   
                                defaultValue= {phone}        
                            />
                    </View>

                    <Text style = {[styles.text_footer,{
                        marginTop: 35
                    }]}>Fächer</Text>

                    <View style = {styles.action}>
                        <FontAwesome
                            name= "envelope"
                            color= '#05375a'
                            size = {20}
                        />
                            <TextInput
                                id = 'subjects'
                                placeholder='Mathe, Deutsch, Englisch'
                                style = {styles.text_input}
                                autoCapitalize='none'
                                onChange={(val) => setSubject(val.target.value)}    
                                defaultValue ={subject}           
                            />   
                    </View>

                    <View style ={styles.button}>
                        <TouchableOpacity
                            onPress={deleteSubmit}
                        >
                            <LinearGradient
                                colors={[ '#c43b5f' , '#ed547c']}
                                style = {[styles.login,{
                                    marginTop: 15
                                }]}
                            >
                                <Text style = {[styles.textSign,{
                                    color: 'white'
                                }]}>Löschen</Text>
                                
                            </LinearGradient>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress = {editSubmit}
                        >
                            <LinearGradient
                                colors={[ '#c43b5f' , '#ed547c']}
                                style = {[styles.login,{
                                    marginTop: 15,
                                    marginLeft: 50
                                }]}
                            >
                                <Text style = {[styles.textSign,{
                                    color: 'white'
                                }]}>Bearbeiten</Text>
                                
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                    <View style = {{
                        marginRight: 20,
                        marginTop: 20
                    }}> 
                        <View >
                            <TouchableOpacity
                                onPress={() => navigation.navigate('LogInScreen')}
                            >
                               
                                    <Text style = {[styles.textSign,{
                                        color: 'black',
                                        fontSize: 18
                                    }]}>Sign-out</Text>
                                    
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style = {styles.action}> 
                        <TextInput
                            style = {styles.text_input}
                        />
                    </View>
                    <View style = {styles.action}> 
                        <TextInput
                            style = {styles.text_input}
                        />
                    </View>
                
                </Animatable.View>
            </ScrollView>
        </SafeAreaView>
</View>
       
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#dbf9f3"
    },
    header: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 60

    },
    footer: {
        flex: 3,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 20,
        paddingHorizontal: 30

    },
    text_header:{
        color: '#03989e',
        fontWeight: 'bold',
        fontSize: 20
    },
    text_footer:{
        color: '#05375a',
        fontSize: 18
    },
    logo:{
        justifyContent: 'flex-end',
        alignItems: 'stretch'
    },
    action:{
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    text_input:{
        flex: 1,
        paddingLeft: 10,
        color: '#05375a'
    },
    button: {
        flexDirection: 'row',
        marginTop: 10
        
    },
    login: {
        width : 150,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },

    textSign:{
        fontSize: 18,
        fontWeight: 'bold'
    },
    textDanger: {
        color: "#dc3545",
    },
 
})

export default studentHomeProfileScreen

