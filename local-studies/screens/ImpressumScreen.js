import React from 'react';
import { StyleSheet, View, Text, StatusBar} from 'react-native';
import * as Animatable from "react-native-animatable";

/**
 * Impressum Seite, in der die wichtige Informationen über die Entwicklerinnen implementiert wurden.
 * @param {*} param0 
 * @returns 
 */

const ImpressumScreen = () => {
    return (
        <View style ={styles.container}>
        <StatusBar backgroundColor=  "#dbf9f3" barStyle='dark-content'/>

            <View style = {styles.header}>
                <Animatable.Image 
                    animation= "bounce"
                    duration={3000}
                    style = {styles.logo} 
                    source={require( "../assets/logo2.png")}
                    resizeMode ="stretch"
                />
            </View>
            <Animatable.View 
                style = {styles.footer}
                animation= "fadeInUpBig"
            >
                <Animatable.Text 
                   animation="rubberBand" duration = {1500} style = {styles.titel}> Local Studies
                </Animatable.Text>
                <Text style = {styles.text}> Aya Abou koura and Lara Porter </Text>
                <Text style = {{marginTop: 30, color: 'grey'}}>This Application is designed to help students to find a suitable Tutor for their situation, that could help them with school problems</Text>
                <Text style = {{ marginTop: 70, color: 'grey', fontSize: 10}}>All Copy Rights go to the designers of this Program</Text>
            </Animatable.View>
        </View>
    );
  }; 
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#dbf9f3"
    },
    header: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center'

    },
    footer: {
        flex: 1,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 50,
        paddingHorizontal: 30

    },

    logo: {
        width: 200,
        height: 200,
    },
    titel:{
        fontSize: 30,
        color: "#c43b5f",
        marginTop: 30
    },
    text:{
        color: '#03989e',
        marginTop: 5

    },
    button:{
        alignItems: 'flex-end',
        marginTop: 30
    },
    textSign:{
        color: 'white',
        fontWeight: 'bold'
    }
})
export default ImpressumScreen