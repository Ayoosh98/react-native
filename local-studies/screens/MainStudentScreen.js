import React from 'react';
import { StyleSheet, View, Text, ScrollView, Image, StatusBar} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as Animatable from "react-native-animatable";
import { SafeAreaView } from 'react-native-safe-area-context';


/**
 * Home Page for students to find a suitable Tutor
 * @param {*} param0 
 * @returns 
 */

const MainStudentScreen = ({navigation}) => {
    return (

        <View style = {styles.container}>
        <StatusBar backgroundColor=  "#dbf9f3" barStyle='dark-content'/>
   
        <SafeAreaView>
            <ScrollView >
                <Animatable.View 
                    animation= "fadeInUpBig"
                    style = {styles.footer}
                > 
                    <Text >
                        <View  style = {[{ flexDirection: 'row'}]}>
                            <Image
                                style = {styles.logo}
                                source={require( "../assets/proband1.jpeg")}
                            />
                            <Text style = {[{  
                                marginLeft: 10,
                                width: '50%',
                                fontSize: 16,
                                color: 'black',
                                flexWrap: 'wrap',
                            }]}>Mein Name ist Ali AlBardan, ich studiere Biomedizintechnik und ich mag gerne anderen in Mathe zu helfen 
                                <TouchableOpacity onPress = {() => navigation.navigate("UserDescriptionScreen")}>
                                    <LinearGradient 
                                        colors={[ 'white' , 'white']}
                                        style = {styles.loginButton}
                                    >
                                    <Text style = {styles.textSign}> Learn More</Text>
                                        <MaterialIcons
                                            name = "navigate-next"
                                            color = 'black'
                                            size = {15}
                                        />
                                    </LinearGradient>
                                </TouchableOpacity>
                            </Text>                               
                        </View>
                    </Text>
                    <Text style= {styles.text}> Ali AlBardan</Text>

                    <Text>
                        <View  style = {[{ flexDirection: 'row'}]}>
                            <Image
                                style = {styles.logo}
                                source={require( "../assets/proband2.jpg")}
                            />
                            <Text style = {[{  
                                marginLeft: 10,
                                width: '50%',
                                fontSize: 16,
                                color: 'black',
                                flexWrap: 'wrap',
                            }]}>Mein Name ist Alice Müller, ich habe Medizin studiert und ich helfe gerne anderen in Biologie
                            
                                <TouchableOpacity >
                                    <LinearGradient 
                                        colors={[ 'white' , 'white']}
                                        style = {styles.loginButton}
                                    >
                                        <Text style = {styles.textSign}> Learn More</Text>
                                            <MaterialIcons
                                                name = "navigate-next"
                                                color = 'black'
                                                size = {15}
                                            
                                            />
                                    </LinearGradient>
                                </TouchableOpacity>
                            </Text>         
                        </View>
                    </Text>
                    <Text style= {styles.text}> Alice Müller</Text>

                    <Text >
                        <View  style = {[{ flexDirection: 'row'}]}>
                            <Image
                                style = {styles.logo}
                                source={require( "../assets/proband3.jpg")}
                            />
                            <Text style = {[{  
                                marginLeft: 10,
                                width: '50%',
                                fontSize: 16,
                                color: 'black',
                                flexWrap: 'wrap',
                            }]}>Mein Name ist Daniel Meier, ich habe Softwaretechnik studiert und ich rechne gerne, sowie ich programmiere gerne.
                            
                                <TouchableOpacity>
                                    <LinearGradient 
                                        colors={[ 'white' , 'white']}
                                        style = {styles.loginButton}
                                    >
                                    <Text style = {styles.textSign}> Learn More</Text>
                                        <MaterialIcons
                                            name = "navigate-next"
                                            color = 'black'
                                            size = {15}
                                        />
                                    </LinearGradient>
                                </TouchableOpacity>
                            </Text>
                        </View>
                    </Text>
                    <Text style= {styles.text}> Daniel Meier</Text>
                </Animatable.View>
                <View style= {{
                    marginLeft: 10
                    }}>
                        <TouchableOpacity
                            onPress={() => navigation.navigate('ImpressumScreen')}
                        >
                            <LinearGradient
                                colors={[ 'White' , 'White']}
                                style = {[styles.login,{
                                    marginTop: 15
                                }]}
                            >
                                <Text style = {[styles.textSign,{
                                    color: 'black',
                                    fontSize: 18
                                }]}>Impressum</Text>
                                
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
            </ScrollView> 
        </SafeAreaView>
    </View>
       
    );
  };

  
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#dbf9f3"
    },
    
    footer: {
        flex: 3,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 50,
        paddingHorizontal: 30

    },
    loginButton: {
        width : 110,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: "row",
        borderRadius: 20,
        marginTop: 20,
        marginRight: 10
    },

    logo: {
        width: 150,
        height: 200,
    },
    titel:{
        fontSize: 30,
        color: "#afc7c2",
        marginTop: 30
    },
    text:{
        color: 'grey',
        marginTop: 5,
        marginBottom: 10

    },
    text_header:{
        color: '#03989e',
        fontWeight: 'bold',
        fontSize: 20
    },

    button:{
        alignItems: 'flex-end',
        marginTop: 30
    },

    textSign:{
        color: 'black',
        fontWeight: 'bold',
    }

})
export default MainStudentScreen;