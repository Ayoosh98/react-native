import React from 'react';
import { StyleSheet, Text, View, StatusBar} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as Animatable from "react-native-animatable";

/**
 * Position Screen ist dafür, um der/die Benutzer*in nach der Registrierung wählen können, ob Sie ein/e Schüler*in oder Tutor*in ist.
 */

const PositionScreen = ({navigation}) => {

    
return (
    <View style = {styles.container}>
        <StatusBar backgroundColor=  "#dbf9f3" barStyle='dark-content'/>

            <View style = {styles.logo_header}>
                        <Animatable.Image 
                            animation= "bounce"
                            duration={3000}
                            style = {styles.logo} 
                            source={require( "../assets/logo2.png")}
                            resizeMode ="stretch"
                        />
            </View>

            <View style = {styles.header}>
                <Text style = {styles.text_header}>Welcome to Local Studies!</Text>
                <Text style = {styles.text_input}>Please choose your Position</Text>
            </View>

                <Animatable.View 
                animation= "fadeInUpBig"
                style = {styles.footer}
                >
                    <View style = {styles.button_view}>
                        <TouchableOpacity
                            onPress= {() => navigation.navigate('ProfileStudentScreen')}
                        >
                            <LinearGradient
                                colors={[ '#ed547c' , '#00c2cb']}
                                style = {styles.student}
                            >
                            
                                <Text style = {styles.textSign}>Student</Text>
                            </LinearGradient>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress= {() => navigation.navigate('ProfileTeacherScreen')}
                        >
                            <LinearGradient
                                colors={[ '#ed547c' , '#00c2cb']}
                                style = {[styles.teacher,{
                                    marginTop: 40
                                }]}
                                >
                                <Text style = {styles.textSign}>Teacher</Text>
                                
                            </LinearGradient>

                        </TouchableOpacity>
                    </View>
                    
                </Animatable.View>
    </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 2,
        backgroundColor: "#dbf9f3"
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50

    },
    logo_header: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 70,

    },
    footer: {
        flex: 2,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 20,
        paddingHorizontal: 30

    },
    text_header:{
        color: '#03989e',
        fontWeight: 'bold',
        fontSize: 30
    },

    text_input:{
        paddingLeft: 10,
        color: 'grey',
        fontSize: 18
    },
    logo:{
        justifyContent: 'center',
        alignItems: 'stretch',
        width: 200,
        height: 200,
    
    },
    student: {
        width : '100%',
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },
    teacher: {
        width : '100%',
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },
    textSign:{
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold'
    },

    button_view:{
        flexDirection: 'column',
        alignItems: 'stretch',
        alignContent: 'space-around',
        marginTop: 50
    }
    


})

export default PositionScreen;