import React from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

import Icons from 'react-native-vector-icons/Ionicons';

import MainStudentScreen from './MainStudentScreen';
import SearchScreen from './SearchScreen';
import ChatScreen from './ChatScreen';
import RatingsScreen from './RatingsScreen';
import studentHomeProfileScreen from './studentHomeProfileScreen';



const Tab = createMaterialBottomTabNavigator();

/**
 * Tab navigator that navigates throught five different screens, home, Search, Profil, Chat, and Rating Screens
 * @param {*} param0 
 * @returns 
 */
const TabScreen = () => {
    return(
   
    <Tab.Navigator
        activeColor='black'
        barStyle= {{
            backgroundColor: '#dbf9f3'                
        }}
    >
            
        <Tab.Screen name= "Home"  component={MainStudentScreen} options={{
            tabBarLabel: 'Home',
            tabBarIcon: () => (
                <Icons name="ios-home"  color={'#03989e'} size={25} />
            ),
        }}/>

        <Tab.Screen name="SearchScreen" component={SearchScreen} options={{
            tabBarLabel: 'Search',
            tabBarIcon: () => (
                <Icons name="ios-search"  color={'#03989e'} size={25} />
            ),
        }}/>
            
        <Tab.Screen name="studentHomeProfileScreen" component={studentHomeProfileScreen} options={{
            tabBarLabel: 'Profil',
            tabBarIcon: () => (
                <Icons name="ios-person"  color={'#03989e'} size={25} />
            ),
        }}/>
             
        <Tab.Screen name="ChatScreen" component={ChatScreen} options={{
            tabBarLabel: 'Chat',
            tabBarIcon: () => (
                <Icons name="ios-chatbox"  color={'#03989e'} size={25} />
            ),
        }}/>
            
        <Tab.Screen name="RatingsScreen" component={RatingsScreen} options={{
            tabBarLabel: 'Rating',
            tabBarIcon: () => (
                <Icons name="ios-star"  color={'#03989e'} size={25} />
            ),
        }}/>

    </Tab.Navigator>
    );
}

export default TabScreen;

