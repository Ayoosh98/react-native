import  React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import RootScreens from './screens/RootScreens';


/**
 * Die App wurde durch RootScreen verbunden
 * @returns 
 */
function App(){
  return (
    <NavigationContainer>
      <RootScreens/>
    </NavigationContainer>

   
     
  );
}




export default App

