var sqlite3 = require('sqlite3').verbose()

const DBSOURCE = 'db.sqlite'

/**
 * Hier wurde eine Tabelle in Datenbanken für profile erstellt
 */
let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
        // Cannot open database
        console.error(err.message)
        throw err
    }

    console.log('Connected to the SQLite database.')
    db.run(
        `CREATE TABLE profiles_teacher(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
        firstName TEXT,
        email TEXT,
        address TEXT,
        money TEXT,
        date_of_birth DATE,
        phonenumber INTEGER,
        subjects TEXT
    )`,  
        (err) => {
            if (err) {
                // table already created
            } else {
                console.log('Connected to the SQLite database.')
                // table just created, creating some rows
                const insert =
                    'INSERT INTO profiles_teacher (name, firstName, email,  address, money, date_of_birth, phonenumber, subjects) VALUES (?, ?, ?, ?, ?, ?, ?, ?)'
                db.run(insert, [
                    'Müller',
                    'Max',
                    'Max@Mustermann.de',
                    'Musterstrasse 1, 11111 Berlin',
                    '10€/h',
                    1996-11-02,
                    01711234567,
                    'English, Mathe, Physik',
                ])
                console.log('inserted user')
            }
        }
    )
})

module.exports = db