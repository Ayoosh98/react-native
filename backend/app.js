const express = require('express')
const bodyParser = require('body-parser')
const db = require('./database')
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
/**
 * Der erste Aufruf von app.use sorgt dafür, dass der Body der Request als JSON-Objekt zur Verfügung steht.
 */
app.use((request, response, next) => {
    response.setHeader('Access-Control-Allow-Origin', '*')
    response.setHeader(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    )
    response.setHeader(
        'Access-Control-Allow-Methods',
        'GET, POST, PATCH, DELETE, OPTIONS'
    )
    next()
})
/** 
 * GET-Funktion zum Einsatz, da lediglich Daten gelesen werden.
*/
app.get('/api/profiles_teacher', (request, response, next) => {
    const sql = 'select * from profiles_teacher'
    const params = []
    db.all(sql, params, (err, profiles_teacher) => {
        if (err) {
            response.status(400).json({
                error: err.message,
            })
            return
        }

        response.status(200).json({
            profiles_teacher: profiles_teacher,
        })
    })
})

/** 
 * GET-Funktion zum Einsatz, da lediglich Daten gelesen werden.
*/
app.get('/api/profiles_teacher/:id', (request, response, next) => {
    const sql = 'select * from profiles_teacher where id = ?'
    const params = [request.params.id]
    db.get(sql, params, (err, profiles_teacher) => {
        if (err) {
            response.status(400).json({
                error: err.message,
            })
            return
        }

        response.status(200).json({
             ...profiles_teacher,
        })
    })
})

/**
 * um ein neues Profil zu erstellen, verwendet man die POST methode
 */

app.post('/api/profiles_teacher', (request, response, next) => {
    const requiredProperties = ['name','firstName', 'email','address', 'money', 'date_of_birth','phonenumber','subjects']
    const errors = []

    requiredProperties.forEach((property) => {
        if (!request.body[property]) {
            errors.push(`Required property ${property} is missing`)
        }
    })

    if (errors.length) {
        response.status(400).json({
            error: errors.join(', '),
        })
        return
    }

    const data = {
        name:request.body.name,
        firstName: request.body.firstName,
        email: request.body.email,
        address: request.body.address,
        money: request.body.money,
        date_of_birth: request.body.date_of_birth,
        phonenumber: request.body.phonenumber,
        subjects: request.body.subjects,

    }

    const sql =
        'INSERT INTO profiles_teacher (name, firstName, email, address, money,date_of_birth, phonenumber, subjects) VALUES (?,?,?,?,?,?,?,?)'
    const params = [data.name, data.firstName, data.email, data.address, data.money, data.date_of_birth, data.phonenumber, data.subjects]
    db.run(sql, params, function (err, result) {
        if (err) {
            response.status(400).json({ error: err.message })
            return
        }
        response.status(201).json({
            message: 'success',
            profile: {
                ...data,
                id: this.lastID,
            },
        })
    })
})

/**
 * Zum Aktualisieren eines Profiles wird das HTTP-Verb PATCH verwendet
 */

app.patch('/api/profiles_teacher/:id', (request, response, next) => {
    const data = {
        name: request.body.name,
        firstName: request.body.firstName,
        email: request.body.email,
        address: request.body.address,
        money: request.body.money,
        date_of_birth: request.body.date_of_birth,
        phonenumber: request.body.phonenumber,
        subjects: request.body.subjects,
    }

    db.run(
        `UPDATE profiles_teacher set
            name = COALESCE(?, name),
            firstName = COALESCE(?, firstName),
            email = COALESCE(?, email),
            address = COALESCE(?, address),
            money = COALESCE(?, money),
            date_of_birth = COALESCE(?, date_of_birth),
            phonenumber = COALESCE(?, phonenumber),
            subjects = COALESCE(?, subjects)`,
        [
            data.name,
            data.firstName,
            data.email,
            data.address,
            data.money,
            data.date_of_birth,
            data.phonenumber,
            data.subjects,
        ],
        function (err, result) {
            if (err) {
                response.status(400).json({
                    error: err.message,
                })
                return
            }
            response.status(200).json({
                profile: data,
                changes: this.changes,
            })
        }
    )
})

/**
 * Um ein Profil zu löschen, kommt das Methode DELETE zum Einsatz:
 */

app.delete('/api/profiles_teacher/:id', (request, response, next) => {
    const sql = 'DELETE FROM profiles_teacher WHERE id = ?'
    const params = [request.params.id]
    db.run(sql, params, function (err, result) {
        if (err) {
            response.status(400).json({ error: err.message })
            return
        }
        response.status(200).json({
            message: 'deleted',
            changes: this.changes,
        })
    })
})


module.exports = app